package utfpr.ct.dainf.if62c.projeto;

import java.util.TimerTask;

/**
 * Linguagem Java
 * @author Zacaron
 */
public class Aviso extends TimerTask {
    
    protected final Compromisso compromisso;

    public Aviso(Compromisso compromisso) {
       this.compromisso = compromisso;
    }

    @Override
    public void run() {
        System.out.println(compromisso.getDescricao() + " começa em " + (compromisso.getData().getTime() - System.currentTimeMillis())/1000 + "s");
   
    }

}
