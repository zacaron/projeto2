package utfpr.ct.dainf.if62c.projeto;

/**
 * Linguagem Java
 * @author Zacaron
 */
public class AvisoFinal extends Aviso {

    public AvisoFinal(Compromisso compromisso) {
        super(compromisso);
    }
    
    @Override
    public void run() {
        System.out.println(compromisso.getDescricao() + " começa agora");
        for(Aviso av : compromisso.getAvisos()) {
            av.cancel();
        }
    }
    
}
